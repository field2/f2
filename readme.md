Hitch
====

What is it?
---
Hitch is a Custom WordPress Theme, probably best categorized as a "starter" theme that is customized for use on our WordPress projects.

Features
---
* Foundation Grid
* UI elements from the Foundation Framework
* HTML5/CSS3 Structure
* Use of HTML5's Figure & Figcaption for WP Captions
* ARIA Compliant
* Basic CSS can be used as a minimalistic theme

In the Pipeline
---
* ARIA Accessibility Updates
* Make it even more lightweight

Credits
---
* Zurb Foundation - http://foundation.zurb.com
* Bones - http://themble.com/bones/